package com.truthsocial.android.codingproject

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.truthsocial.android.codingproject.data.model.Post
import com.truthsocial.android.codingproject.data.repository.PostRepository
import kotlinx.coroutines.flow.Flow

class PublicationsViewModel(repository: PostRepository): ViewModel() {

       private val repository by lazy { repository }

    fun getPost(): Flow<Collection<Post>> {
        return repository.getPosts(1,50)
    }

    fun likePost(postId: String):Boolean{
        return repository.likePost(postId)
    }

    fun createPost(body: String): Post? {
        return repository.createPost(body)
    }



}