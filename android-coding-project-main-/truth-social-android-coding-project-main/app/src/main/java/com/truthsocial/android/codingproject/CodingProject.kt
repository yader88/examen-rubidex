package com.truthsocial.android.codingproject

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CodingProject : Application()