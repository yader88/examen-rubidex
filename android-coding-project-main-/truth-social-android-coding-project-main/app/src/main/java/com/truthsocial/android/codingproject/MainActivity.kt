package com.truthsocial.android.codingproject

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AddCircle
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExtendedFloatingActionButton
import androidx.compose.material3.FabPosition
import androidx.compose.material3.FloatingActionButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.truthsocial.android.codingproject.data.model.Post
import com.truthsocial.android.codingproject.ui.theme.TruthSocialCodingProjectTheme
import kotlinx.coroutines.flow.map

class MainActivity : ComponentActivity() {
val publicationsViweModel by viewModels<PublicationsViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        enableEdgeToEdge()

        setContent {
            TruthSocialCodingProjectTheme {
                var listPub : List<Post> = listOf()
                publicationsViweModel.getPost().map { publications->
                    listPub = publications.toList()

                }
                Scaffold(
                    modifier = Modifier.fillMaxSize(),
                    floatingActionButtonPosition = FabPosition.Center,
                    floatingActionButton = {AddPublication(publicationsViweModel)} ) { innerPadding ->

                    /*val listadoPublicaciones = remember {
                        listOf(
                            Post("1", "1","jjjj", 1),
                            Post("2", "1","jjjj", 1),
                            Post("3", "1","jjjj", 1),
                        )
                    }*/
                    PublicationList(listadoPublicaciones = listPub, publicationsViweModel)

                }
            }
        }
    }
}

@Composable
fun PublicationList(listadoPublicaciones: List<Post>, viewmodel: PublicationsViewModel){
    LazyColumn(
        contentPadding = PaddingValues(
            horizontal = 16.dp,
            vertical = 8.dp)
    ) {
        items(listadoPublicaciones){publication ->
            CardPublication(publication = publication, viewmodel)
        }

    }
}

@Composable
fun CardPublication(publication: Post, viewmodel: PublicationsViewModel){
val context = LocalContext.current
    Card (
        modifier = Modifier
            .padding(horizontal = 12.dp, vertical = 12.dp)
            .fillMaxWidth(),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 6.dp),
        colors = CardDefaults.cardColors(
            containerColor = Color.LightGray,
            contentColor = Color.Black
        ),
        shape = RoundedCornerShape(corner = CornerSize(16.dp))
    ){
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(20.dp),
            Arrangement.Center
        ) {
            Text(text = publication.userId,
                 style = TextStyle(
                     color = Color.Black,
                     fontSize = 20.sp,
                     fontWeight = FontWeight.Bold
                 ),
                 modifier = Modifier.align(Alignment.CenterHorizontally)
            )
            Text(text = publication.body,
                 style = TextStyle(
                     color = Color.Black,
                     fontSize = 16.sp,
                 ),
                 modifier = Modifier.align(Alignment.Start)
            )
            Row(verticalAlignment = Alignment.CenterVertically) {
                Text(text = publication.likes.toString(),
                     style = TextStyle(
                         color = Color.Black,
                         fontSize = 16.sp,
                     )
                )
                IconButton(onClick = {
                    viewmodel.likePost(publication.id)
                }) {
                    Icon(Icons.Filled.Favorite,
                         contentDescription = "Like",
                         tint = Color.Red,
                         modifier = Modifier.size(20.dp))
                }
            }
        }

    }
}

@Composable
fun AddPublication(viewmodel: PublicationsViewModel){
    var showDialogDatos by remember {
        mutableStateOf(false)
    }
    var pub by remember {
        mutableStateOf("")
    }
    if (showDialogDatos){
        AlertDialog(
            onDismissRequest = { showDialogDatos = false},
            confirmButton = { TextButton(onClick = {
                viewmodel.createPost(pub)
                showDialogDatos=false}) {
                Text(text = "Agregar")
            } },
            dismissButton = { TextButton(onClick = { showDialogDatos=false }) {
                Text(text = "Cancelar")
            }},
            title = { Text(text = "Agregue una nueva publicacion")},
            text = {
                Column {
                    TextField(
                        value = pub,
                        onValueChange = { pub = it },
                        placeholder = {
                            Text(text = "publicacion")
                        })
                }
            })
    }
    ExtendedFloatingActionButton(
        text = { Text(text = "Agregar") },
        icon = { Icon(imageVector = Icons.Default.AddCircle, contentDescription = "Add publication") },
        onClick = { showDialogDatos = true },
        elevation = FloatingActionButtonDefaults.elevation(16.dp))
}



