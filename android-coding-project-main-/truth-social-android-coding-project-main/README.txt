Truth Social - Android Coding Project

In this exercise you will complete a very basic version of a social media feed.
We've provided this starter project containing a PostRepository which you will use to provide the feed with data along with
a project skeleton from which you start to build the screen. Some 3rd party libraries are already included
but you may add additional libraries you feel are needed.

Requirements:
- Build a screen which displays a list of Posts using the data provided by PostRepository.getPosts().
- Each Post must display:
    -The author's user name
    -The post body
    -The bottom row should display the "likes" count and a "Like" button.
- Tapping the like button should increment the like count using PostRepository.likePost( ... )
- You should not need to modify any code under the datarepository module.

Bonus:
- Create a simple screen to add a new post using PostRepository.createPost

Please use any architectural patterns you would use in a regular project
such as ViewModels/MVVM, Dependency Injection, etc., even if it may seem
like overkill for this single screen. We're interested in your knowledge
of various design patterns.




