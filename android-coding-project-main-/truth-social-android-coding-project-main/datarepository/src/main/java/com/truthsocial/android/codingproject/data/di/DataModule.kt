package com.truthsocial.android.codingproject.data.di

import com.truthsocial.android.codingproject.data.repository.PostRepository
import com.truthsocial.android.codingproject.data.repository.PostRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class DataModule {

    @Binds
    internal abstract fun bindPostRepository(postRepositoryImpl: PostRepositoryImpl): PostRepository

}