package com.truthsocial.android.codingproject.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Post(
    val id: String,
    val userId: String,
    val body: String,
    val likes: Int = 0,
) : Parcelable