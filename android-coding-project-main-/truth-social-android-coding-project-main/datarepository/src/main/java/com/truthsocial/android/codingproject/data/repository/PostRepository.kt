package com.truthsocial.android.codingproject.data.repository

import com.truthsocial.android.codingproject.data.model.Post
import com.truthsocial.android.codingproject.data.util.randomId
import io.bloco.faker.Faker
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.flow.updateAndGet
import javax.inject.Inject


interface PostRepository {
    fun getPosts(offset: Int, limit: Int): Flow<Collection<Post>>
    fun likePost(postId: String): Boolean
    fun createPost(body: String): Post?
}

internal class PostRepositoryImpl @Inject constructor(
    private val faker: Faker,
) : PostRepository {

    private val postsCache: MutableStateFlow<Map<String, Post>> = MutableStateFlow(mapOf())
    private val posts: Flow<Collection<Post>> = postsCache.map { it.values }

    init {
        postsCache.update { posts ->
            posts.toMutableMap().apply {
                repeat(100) {
                    computeIfAbsent(faker.randomId) { postId ->
                        Post(
                            id = postId,
                            userId = faker.randomId,
                            body = faker.lorem.paragraph(),
                            likes = (0..4).random().let {
                                if (it == 0) 0
                                else faker.number.positive(0, 999_999)
                            },
                        )
                    }
                }
            }
        }
    }

    override fun getPosts(
        offset: Int,
        limit: Int,
    ): Flow<Collection<Post>> {
        require(offset >= 0)
        require(limit > 0)

        return posts
            .map { it.drop(offset).take(limit) }
            .onStart { delay((250L..2000L).random()) }
    }

    override fun likePost(postId: String): Boolean {
        if (faker.bool.bool(1 / 20f)) // introduce a failure rate.
            return false

        return postsCache.updateAndGet { map ->
            val updatedPost = map[postId]
                ?.run { copy(likes = likes + 1) }
                ?: return@updateAndGet map

            map + (postId to updatedPost)
        }.contains(postId)
    }

    override fun createPost(body: String): Post? {
        if (faker.bool.bool(1 / 20f)) // introduce a failure rate.
            return null

        val newPost = Post(
            id = faker.randomId,
            userId = faker.randomId,
            body = body,
            likes = 0,
        )

        postsCache.update {
            it + (newPost.id to newPost)
        }

        return newPost
    }
}