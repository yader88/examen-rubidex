package com.truthsocial.android.codingproject.data.util

import io.bloco.faker.Faker

val Faker.randomId
    get() = number.number(9)