package com.truthsocial.android.codingproject.data.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.bloco.faker.Faker

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    fun provideFaker(): Faker {
        return Faker()
    }

}